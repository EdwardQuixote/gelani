package com.mitchell.gelani;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mitchell.gelani.Gelani_Background.DB_Worker;
import com.mitchell.gelani.Gelani_Misc.Gelani_Codes;

public class SignUpActivity extends AppCompatActivity {

    private TextInputLayout tilFullName;
    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;

    private EditText edFullName;
    private EditText edEmail;
    private EditText edPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to initialize class Variables and,
     * Objects used in this Activity's UI.
     *
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        tilFullName = (TextInputLayout) this.findViewById(R.id.tilSUFullName);
        tilEmail = (TextInputLayout) this.findViewById(R.id.tilSUEmailAddress);
        tilPassword = (TextInputLayout) this.findViewById(R.id.tilSUPassword);

        edFullName = (EditText) this.findViewById(R.id.edSUFullName);
        edEmail = (EditText) this.findViewById(R.id.edSUEmailAddress);
        edPassword = (EditText) this.findViewById(R.id.edSUPassword);

        TextView txtSignIn = (TextView) this.findViewById(R.id.txtSUSignIn);
        txtSignIn.setOnClickListener(clkSignUp);

        Button btnSignUp = (Button) this.findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(clkSignUp);
    }

    /**
     * Method to check what user provided,
     * and validate if it's correct.
     *
     * Called in this.clkSignUp.onClick();
     */
    private void codeToValidateUserProvidedDetails() {

        String sTILError;

        if (edFullName.getText().toString().equalsIgnoreCase("")) {
            sTILError = this.getResources().getString(R.string.tileEmptyField) + " your Full Name!";
            tilFullName.setError(sTILError);

            edFullName.requestFocus();
        } else if (edEmail.getText().toString().equalsIgnoreCase("")) {
            sTILError = this.getResources().getString(R.string.tileEmptyField) + " your Email Address!";
            tilEmail.setError(sTILError);

            edEmail.requestFocus();
        } else if (edPassword.getText().toString().equalsIgnoreCase("")) {
            sTILError = this.getResources().getString(R.string.tileEmptyField) + " your Password!";
            tilPassword.setError(sTILError);

            edPassword.requestFocus();
        } else {
            tilFullName.setError("");
            tilEmail.setError("");
            tilPassword.setError("");

            codeToRegisterNewUserAccount();
        }
    }

    /**
     * Method to save User Details and register new Account,
     * in the Database.
     *
     * Called in this.codeToValidateUserProvidedDetails();
     */
    private void codeToRegisterNewUserAccount() {

        String sFullName = edFullName.getText().toString();
        String sEmail = edEmail.getText().toString();
        String sPassword = edPassword.getText().toString();

        DB_Worker clsWorker = new DB_Worker(SignUpActivity.this);
        clsWorker.execute(Gelani_Codes.sEXECUTETAG_SIGNUP, sFullName, sEmail, sPassword);

        edFullName.setText("");
        edEmail.setText("");
        edPassword.setText("");
    }


    /**
     * View.OnClickListener interface to consume clicks for views,
     * in this Activity.
     * It first takes the id of the View that was clicked,
     * then compares it to the Views of this Activity.
     * Case R.id.btnSignUp:
     * This signs up a user to the Database.
     *
     * Case R.id.txtSUSignIn:
     * This Starts the SIgn In Activity: SignInActivity.java
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkSignUp = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int iViewId = v.getId();
            switch (iViewId) {
                case R.id.btnSignUp:
                    codeToValidateUserProvidedDetails();
                    break;

                case R.id.txtSUSignIn:
                    Intent inStartSignIn = new Intent(SignUpActivity.this, SignInActivity.class);
                    finish();
                    startActivity(inStartSignIn);
            }
        }
    };
}
