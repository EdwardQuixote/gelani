package com.mitchell.gelani;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mitchell.gelani.Gelani_Dialogs.G_Dialogs;
import com.mitchell.gelani.Gelani_Misc.GelaniPlayServices;
import com.mitchell.gelani.Gelani_Misc.Gelani_Codes;

public class MapActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final static LatLng ltlnNAIROBI = new LatLng(-1.292065900000000000, 36.821946199999960000);    //	Location Coordinates for NAIROBI City, Kenya

    private GelaniPlayServices clsPlayServices;
    private G_Dialogs clsDialogs;

    private DrawerLayout dlayDrawer;

    private NavigationView navDrawer;

    private ActionBarDrawerToggle abdtDrawer;

    private GoogleMap gmMap;
    private GoogleApiClient gacAPIClient;
    private GoogleMapOptions gmoMapOptions;
    private CameraPosition cmpDefault;
    private Marker mkrUserLocation;

    private FusedLocationProviderApi flpaLocationProvider;
    private LocationManager lmLocManager;
    private LocationRequest lrqLocationRequest;
    private Location locUserLocation;
    private LatLng ltlnUserLocation;
    private CameraPosition cmpDynamic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        initializeVariablesAndUIObjects();

        codeToCheckIfGooglePlayServicesExist();

    }


    /**
     * Method to initialize Class Variables,
     * and Objects used in the UI.
     *
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        clsPlayServices = new GelaniPlayServices(
                MapActivity.this,
                MapActivity.this,
                Gelani_Codes.iGPLAY_SERVICES_CONNECTION_REQUEST_CODE,
                Gelani_Codes.iGPLAY_SERVICES_CONNECTION_STATUS_CODE);
        clsDialogs = new G_Dialogs(MapActivity.this);

        gacAPIClient = new GoogleApiClient.Builder(this, this, this)
                .addApi(LocationServices.API)
                .build();

        flpaLocationProvider = LocationServices.FusedLocationApi;
        lmLocManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        cmpDefault = new CameraPosition(ltlnNAIROBI, 10f, 0f, 0f);
        gmoMapOptions = new GoogleMapOptions()
                .compassEnabled(true)
                .zoomControlsEnabled(true)
                .zoomGesturesEnabled(true)
                .tiltGesturesEnabled(true)
                .rotateGesturesEnabled(true);

        dlayDrawer = (DrawerLayout) this.findViewById(R.id.dlayMap);

        navDrawer = (NavigationView) this.findViewById(R.id.navMapDrawer);
        navDrawer.setNavigationItemSelectedListener(nislDrawer);

        codeToSetUpNavigationDrawer();

        Button btnGetACab = (Button) this.findViewById(R.id.btnMapGetACab);
        btnGetACab.setOnClickListener(clkMap);
    }

    /**
     * Method to set up Navigation Drawer on Layout.
     *
     * Called in this.initializeVariablesAndUIObjects();
     */
    private void codeToSetUpNavigationDrawer() {

        abdtDrawer = new ActionBarDrawerToggle(MapActivity.this, dlayDrawer, null, R.string.sDrawerOpen, R.string.sDrawerClose) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                supportInvalidateOptionsMenu();

            }

        };
        abdtDrawer.setHomeAsUpIndicator(R.drawable.ic_drawer);
        dlayDrawer.setDrawerListener(abdtDrawer);

    }

    /**
     * Method to check if Google Play Services exist in Device,
     * so as to Launch Map
     *
     * Called in onCreate();
     */
    private void codeToCheckIfGooglePlayServicesExist() {

        boolean boolIsPlayAvailable = clsPlayServices.codeToCheckIfGooglePlayServicesExist();
        if (boolIsPlayAvailable) {

            gacAPIClient.connect();

            codeToSetUpMap();
            codeToCheckIfGPSAndNetworkLocationProvidersAreActivated();

        } else {
            Toast.makeText(MapActivity.this, R.string.tstNoGPlayServices, Toast.LENGTH_LONG).show();
        }

    }

    /**
     * This method checks first if the GoogleApiClient being used is Connected,
     * to GooglePlayServices.
     * If it's connected,
     * it then checks if Location GPS and Network Provider is enabled.
     * If not,
     * it displays a dialog about it.
     *
     * Called in this.codeToCheckIfGooglePlayServicesExist().
     */
    private void codeToCheckIfGPSAndNetworkLocationProvidersAreActivated() {

        String sDialogMessage;
        if (gacAPIClient.isConnected()) {
            if (!lmLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                sDialogMessage = this.getString(R.string.dgLocGPSMessage);
                clsDialogs.codeToCreateAlertDialogs(Gelani_Codes.iLOCATION_DIALOG, sDialogMessage);
            } else if (!lmLocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                sDialogMessage = this.getString(R.string.dgLocNetMessage);
                clsDialogs.codeToCreateAlertDialogs(Gelani_Codes.iLOCATION_DIALOG, sDialogMessage);
            }
        }

    }

    /**
     * This Method Gets the Map from Google Servers,
     * and Loads it on the App.
     * If Location is not null,
     * It sets up a Marker on the User's Location,
     * once the Map has Loaded.
     *
     * Called in codeToCheckIfGooglePlayServicesExist();
     */
    private void codeToSetUpMap() {

        SupportMapFragment.newInstance(gmoMapOptions);
        if (gmMap == null) {
            gmMap = ((SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.fragMap)).getMap();

            if (gmMap != null) {
                if (locUserLocation != null) {
                    codeToSetUpUserLocationMarker(locUserLocation);
                }
            }
        } else {
            if (locUserLocation != null) {
                codeToSetUpUserLocationMarker(locUserLocation);
            }
        }

        gmMap.animateCamera(CameraUpdateFactory.newCameraPosition(cmpDefault));

    }

    /**
     * Method to Set Up Main Marker(Color:HUE_RED),
     * on User's Location.
     * This Marker shows user's Location.
     *
     * Called in codeToSetUpMap();
     */
    private void codeToSetUpUserLocationMarker(Location location) {

        ltlnUserLocation = new LatLng(location.getLatitude(), location.getLongitude());

        gmMap.addMarker(new MarkerOptions().position(ltlnUserLocation)
                .title(this.getResources().getString(R.string.sYourLocation))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

    }

    /**
     * This method basically gets Location from the LocationClient Service.
     *
     * Called in this.onConnected(), this.onOptionsItemSelected().mnuMapMyLocation
     */
    private void codeToGetUserLocation() {

        locUserLocation = flpaLocationProvider.getLastLocation(gacAPIClient);
        if (locUserLocation != null) {
            codeToUpdateUserCurrentLocation(locUserLocation);
        } else {
            Toast.makeText(MapActivity.this, R.string.tstWaitLocation, Toast.LENGTH_LONG).show();
        }

    }

    /**
     * This Method is called in LocationManagerService.class
     * It updates Location of User Marker.
     *
     * Called in LocationManagerService.class.onLocationChanged();
     */
    public void codeToUpdateUserCurrentLocation(Location location) {

        this.locUserLocation = location;

        ltlnUserLocation = new LatLng(locUserLocation.getLatitude(), locUserLocation.getLongitude());
        cmpDynamic = new CameraPosition(ltlnUserLocation, 14f, 0f, 0f);
        gmMap.animateCamera(CameraUpdateFactory.newCameraPosition(cmpDynamic));

        codeToSetUpUserLocationMarker(locUserLocation);

    }


    /**
     * OnNavigationItemSelectedListener for NavigationDrawer in this Activity.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private NavigationView.OnNavigationItemSelectedListener nislDrawer = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {

            int iItemId = item.getItemId();
            switch (iItemId) {
                case R.id.mnuDrawerAboutUs: //  About Us
                    dlayDrawer.closeDrawer(navDrawer);

                    startActivity(new Intent(MapActivity.this, AboutUsActivity.class));
                    break;

                case R.id.mnuDrawerGetStarted: //  Get Started
                    dlayDrawer.closeDrawer(navDrawer);

                    Intent inStartGetStarted = new Intent(MapActivity.this, GetStartedActivity.class);
                    startActivity(inStartGetStarted);
                    break;

                case R.id.mnuDrawerMap: //  Map
                    dlayDrawer.closeDrawer(navDrawer);
                    break;
            }
            return false;
        }
    };

    private Button.OnClickListener clkMap = new Button.OnClickListener() {

        @Override
        public void onClick(View view) {

            int iViewId = view.getId();
            switch (iViewId) {
                case R.id.btnMapGetACab:
                    startActivity(new Intent(MapActivity.this, GetStartedActivity.class));
                    break;
            }
        }
    };


    @Override
    public void onBackPressed() {

        if (dlayDrawer.isDrawerVisible(navDrawer)) {
            dlayDrawer.closeDrawer(navDrawer);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        abdtDrawer.onConfigurationChanged(newConfig);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        abdtDrawer.syncState();

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (gacAPIClient != null) {
            if (!gacAPIClient.isConnected() || !gacAPIClient.isConnecting()) {
                gacAPIClient.connect();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (lrqLocationRequest != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(gacAPIClient, this);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (gacAPIClient != null) {
            if (gacAPIClient.isConnected() || gacAPIClient.isConnecting()) {
                gacAPIClient.disconnect();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_map, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        boolean boolDrawerIsOpen = dlayDrawer.isDrawerVisible(navDrawer);

        menu.findItem(R.id.mnuMapType).setVisible(!boolDrawerIsOpen);
        menu.findItem(R.id.mnuMapMyLocation).setVisible(!boolDrawerIsOpen);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int iItemId = menuItem.getItemId();
        switch (iItemId) {
            case R.id.mnuMapTypeNormal:
                gmMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;

            case R.id.mnuMapTypeTerrain:
                gmMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;

            case R.id.mnuMapTypeSatellite:
                gmMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;

            case R.id.mnuMapTypeHybrid:
                gmMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;

            case R.id.mnuMapMyLocation:
                codeToGetUserLocation();
                return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onConnected(Bundle bundle) {

        lrqLocationRequest = LocationRequest.create();
        lrqLocationRequest.setInterval(Gelani_Codes.iUPDATE_INTERVAL);
        lrqLocationRequest.setFastestInterval(Gelani_Codes.iFASTEST_UPDATE_INTERVAL);
        lrqLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(gacAPIClient, lrqLocationRequest, this);

        codeToGetUserLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onLocationChanged(Location location) {

        codeToSetUpUserLocationMarker(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Toast.makeText(MapActivity.this, R.string.tstLocClientFailed, Toast.LENGTH_SHORT).show();

        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(MapActivity.this, Gelani_Codes.iGPLAY_SERVICES_CONNECTION_REQUEST_CODE);
            } catch (IntentSender.SendIntentException siex) {
                siex.printStackTrace();
            }
        }

    }
}
