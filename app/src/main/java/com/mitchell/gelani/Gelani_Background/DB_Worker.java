package com.mitchell.gelani.Gelani_Background;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.mitchell.gelani.Gelani_Dialogs.G_Dialogs;
import com.mitchell.gelani.Gelani_Misc.Gelani_Codes;
import com.mitchell.gelani.MapActivity;
import com.mitchell.gelani.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Edward Quixote on 30/03/2016.
 */
public class DB_Worker extends AsyncTask<String, Void, String> {

    private G_Dialogs clsDialogs;

    private Context coxContext;

    public DB_Worker(Context context) {
        this.coxContext = context;

        clsDialogs = new G_Dialogs(coxContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        clsDialogs.codeToCreateProgressDialogs(coxContext.getResources().getString(R.string.sLoading));
    }

    @Override
    protected String doInBackground(String... params) {

        String sExecute_Tag = params[0];
        if (sExecute_Tag.equals(Gelani_Codes.sEXECUTETAG_SIGNUP)) { //  Sign Up
            String sFullName = params[1];
            String sEmailAddress = params[2];
            String sPassword = params[3];

            try {
                URL urlSignUp = new URL(Gelani_Codes.sURL_SIGNUP);
                HttpURLConnection hucConnection = (HttpURLConnection) urlSignUp.openConnection();
                hucConnection.setRequestMethod(Gelani_Codes.sPOST_REQUEST);
                hucConnection.setDoOutput(true);

                Log.w(DB_Worker.this.toString() + "SIGN UP LOG: ", "CONNECTION ESTABLISHED!");

                OutputStream osSignUp = hucConnection.getOutputStream();
                BufferedWriter bfwSignUp = new BufferedWriter(new OutputStreamWriter(osSignUp, Gelani_Codes.sENCODING_FORMAT));
                String sSignUpData =
                        URLEncoder.encode("full_name", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sFullName, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("email_address", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sEmailAddress, Gelani_Codes.sENCODING_FORMAT) + " &" +
                        URLEncoder.encode("password", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sPassword, Gelani_Codes.sENCODING_FORMAT);
                bfwSignUp.write(sSignUpData);

                Log.w(DB_Worker.this.toString() + "LOG: bfwSignUp: ", bfwSignUp.toString());

                bfwSignUp.flush();
                bfwSignUp.close();
                osSignUp.close();

                InputStream isServerResponse = hucConnection.getInputStream();
                BufferedReader bfrSignUp = new BufferedReader(new InputStreamReader(isServerResponse, Gelani_Codes.sDECODING_FORMAT));
                String sServerResponse = "";
                String sFileLine;
                while ((sFileLine = bfrSignUp.readLine()) != null) {
                    sServerResponse += sFileLine;
                }

                Log.w(DB_Worker.this.toString() + "LOG: sServerResponse: ", sServerResponse);

                isServerResponse.close();

                String sSignUpResponse = "";
                if (sServerResponse.equalsIgnoreCase("Sign Up UNSUCCESSFUL!")) {
                    sSignUpResponse = "Sign Up UNSUCCESSFUL!";
                } else {
                    sSignUpResponse = "Sign Up SUCCESSFUL!";
                }

                Log.w(DB_Worker.this.toString() + "LOG: sSignUpResponse: ", sSignUpResponse);

                hucConnection.disconnect();
                return sSignUpResponse;
            } catch (IOException ioex) {
                Log.e(DB_Worker.this.toString() + " IOEXCEPTION: ", ioex.toString());
            }
        } else if (sExecute_Tag.equals(Gelani_Codes.sEXECUTETAG_SIGNIN)) {  //  Sign In
            String sEmailAddress = params[1];
            String sPassword = params[2];

            try {
                URL urlSignIn = new URL(Gelani_Codes.sURL_SIGNIN);
                HttpURLConnection hucSignInConnection = (HttpURLConnection) urlSignIn.openConnection();
                hucSignInConnection.setRequestMethod(Gelani_Codes.sPOST_REQUEST);
                hucSignInConnection.setDoOutput(true);
                hucSignInConnection.setDoInput(true);

                OutputStream osSignIn = hucSignInConnection.getOutputStream();
                BufferedWriter bfwSignIn = new BufferedWriter(new OutputStreamWriter(osSignIn, Gelani_Codes.sENCODING_FORMAT));
                String sSignInData =
                        URLEncoder.encode("email_address", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sEmailAddress, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("password", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sPassword, Gelani_Codes.sENCODING_FORMAT);
                bfwSignIn.write(sSignInData);
                bfwSignIn.flush();
                bfwSignIn.close();
                osSignIn.close();

                InputStream isSignIn = hucSignInConnection.getInputStream();
                BufferedReader bfrSignIn = new BufferedReader(new InputStreamReader(isSignIn, Gelani_Codes.sDECODING_FORMAT));
                String sServerResponse = "";
                String sFileLine;
                while ((sFileLine = bfrSignIn.readLine()) != null) {
                    sServerResponse += sFileLine;
                }
                bfrSignIn.close();
                isSignIn.close();

                String sSignInResponse = "";
                if (sServerResponse.equalsIgnoreCase("Login UNSUCCESSFUL!")) {
                    hucSignInConnection.disconnect();

                    sSignInResponse = "Login UNSUCCESSFUL!";
                } else {
                    hucSignInConnection.disconnect();

                    sSignInResponse = "Login SUCCESSFUL!";
                }

                return sSignInResponse;
            } catch (IOException ioex) {
                Log.e(DB_Worker.this.toString() + " IOEXCEPTION: ", ioex.toString());
            }
        } else if (sExecute_Tag.equals(Gelani_Codes.sEXECUTETAG_RESERVEACAB)) {
            String sDestination = params[1];
            String sPickUpPoint = params[2];
            String sCabCompany = params[3];
            String sCabType = params[4];
            String sPaymentMethod = params[5];
            String sCharges = params[6];
            String sPhoneNumber = params[7];

            try {
                URL urlReserveACab = new URL(Gelani_Codes.sURL_RESERVEACAB);
                HttpURLConnection hucReserveACab = (HttpURLConnection) urlReserveACab.openConnection();
                hucReserveACab.setRequestMethod(Gelani_Codes.sPOST_REQUEST);
                hucReserveACab.setDoOutput(true);

                OutputStream osReserveACab = hucReserveACab.getOutputStream();
                BufferedWriter bfwReserveACab = new BufferedWriter(new OutputStreamWriter(osReserveACab, Gelani_Codes.sENCODING_FORMAT));
                String sReserveACabData =
                        URLEncoder.encode("destination", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sDestination, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("pick_up_point", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sPickUpPoint, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("cab_company", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sCabCompany, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("cab_type", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sCabType, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("payment_method", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sPaymentMethod, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("charges", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sCharges, Gelani_Codes.sENCODING_FORMAT) + " & " +
                        URLEncoder.encode("phone_number", Gelani_Codes.sENCODING_FORMAT) + "=" + URLEncoder.encode(sPhoneNumber, Gelani_Codes.sENCODING_FORMAT);
                bfwReserveACab.write(sReserveACabData);
                bfwReserveACab.flush();
                bfwReserveACab.close();
                osReserveACab.close();

                InputStream isReserveACab = hucReserveACab.getInputStream();
                BufferedReader bfrReserveACab = new BufferedReader(new InputStreamReader(isReserveACab, Gelani_Codes.sDECODING_FORMAT));
                String sServerResponse = "";
                String sFileLine;
                while ((sFileLine = bfrReserveACab.readLine()) != null) {
                    sServerResponse += sFileLine;
                }
                bfrReserveACab.close();
                isReserveACab.close();

                hucReserveACab.disconnect();

                return sServerResponse;
//                return "Reservation SUCCESSFUL";  //  TODO: Remove if unnecessary
            } catch (IOException ioex) {
                Log.e(DB_Worker.this.toString() + " IOEXCEPTION: ", ioex.toString());
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        clsDialogs.codeToDismissProgressDialogs();

        if (s != null) {
            if (s.equalsIgnoreCase("Sign Up SUCCESSFUL!")) {
                Toast.makeText(coxContext, s, Toast.LENGTH_LONG).show();

                Intent inStartMapActivity = new Intent(coxContext, MapActivity.class);
                coxContext.startActivity(inStartMapActivity);   //  TODO; CONTINUE FROM HERE. Test this.
            } else if (s.equals("Reservation SUCCESSFUL")) {    //  Case for Sign Up and Reserve a Cab
                Toast.makeText(coxContext, s, Toast.LENGTH_LONG).show();
            } else if (s.equalsIgnoreCase("")) {
                Toast.makeText(coxContext, "Can't reach the Server. Please check your network!", Toast.LENGTH_LONG).show();

                Log.w(DB_Worker.this.toString() + " LOG: ", s);
            } else {    //  Case for Sign In response
                Log.w(DB_Worker.this.toString() + " LOG: ", s);

                if (s.equalsIgnoreCase("Login SUCCESSFUL!")) {
                    Intent inStartMapActivity = new Intent(coxContext, MapActivity.class);
                    coxContext.startActivity(inStartMapActivity);
                }   //  TODO: Test this.

                Toast.makeText(coxContext, s, Toast.LENGTH_LONG).show();
            }
        } else {
            Log.w(DB_Worker.this.toString() + " LOG: ", "S IS NULL");
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

//        clsDialogs.codeToDismissProgressDialogs();
    }
}
