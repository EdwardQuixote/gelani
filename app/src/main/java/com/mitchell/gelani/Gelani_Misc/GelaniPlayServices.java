package com.mitchell.gelani.Gelani_Misc;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Edward Quixote on 29/03/2016.
 */
public class GelaniPlayServices extends FragmentActivity {

    private GooglePlayServicesErrorDialogFragment clsDialogFragment;

    private Dialog dgError;

    private Context coxContext;
    private Activity actActivity;

    private ConnectionResult corConResult;

    private PendingIntent piIntent;

    private int iConRequestCode;
    private int iConStatusCode;
    private int iConErrorCode;

    private boolean boolIsAvailable;

    public GelaniPlayServices(Context context, Activity activity, int requestCode, int statusCode) {

        this.coxContext = context;
        this.actActivity = activity;

        this.iConRequestCode = requestCode;
        this.iConStatusCode = statusCode;

        clsDialogFragment = new GooglePlayServicesErrorDialogFragment();

        corConResult = new ConnectionResult(iConStatusCode, piIntent);

    }


    /**
     * Method to check if Services Exist.
     * If it exists, it return boolIsAvailable as TRUE
     * else,
     * as FALSE.
     *
     * @return boolIsAvailable (boolean)
     */
    public boolean codeToCheckIfGooglePlayServicesExist() {

        int iConResultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(coxContext);
        if (iConResultCode == ConnectionResult.SUCCESS) {

            boolIsAvailable = true;

            return boolIsAvailable;
        } else {

            if (corConResult.hasResolution() == true) {

                try {

                    corConResult.startResolutionForResult(actActivity, iConRequestCode);

                } catch (IntentSender.SendIntentException siex) {

                    Logger.getLogger("Google_Play_Services_Error").log(Level.SEVERE, "SenderIntentException Occured! " + siex.toString());

                }

            } else {

                iConErrorCode = corConResult.getErrorCode();

                dgError = GooglePlayServicesUtil.getErrorDialog(iConErrorCode, actActivity, iConRequestCode);
                if (dgError != null) {

                    clsDialogFragment.codeToSetUpDialog(dgError);

                }

            }

            boolIsAvailable = false;

            return boolIsAvailable;
        }

    }

    public static class GooglePlayServicesErrorDialogFragment extends DialogFragment {

        private Dialog dgDialog;

        public GooglePlayServicesErrorDialogFragment() {
            super();

            dgDialog = null;

        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            return dgDialog;
        }


        /**
         * Code to Set the Passed Dialog to our DialogFragment
         *
         * @param dialog (Dialog)
         */
        public void codeToSetUpDialog(Dialog dialog) {

            this. dgDialog = dialog;

        }

    }
}
