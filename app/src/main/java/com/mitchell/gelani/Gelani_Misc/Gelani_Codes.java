package com.mitchell.gelani.Gelani_Misc;

/**
 * Created by Edward Quixote,
 * on 29/03/2016.
 */
public class Gelani_Codes {

    public static final String sURL_SIGNUP = "http://10.10.12.218/Gelani/signup_gelani.php";
    public static final String sURL_SIGNIN = "http://10.10.12.218/Gelani/gelani_signin.php";
    public static final String sURL_RESERVEACAB = "http://10.10.12.218/Gelani/reserveACab_Gelani.php";
    public final static String sENCODING_FORMAT = "UTF-8";
    public final static String sDECODING_FORMAT = "iso-8859-1";
    public final static String sEXECUTETAG_SIGNUP = "com.mitchell.gelani.Gelani_Misc.SIGN_UP";
    public final static String sEXECUTETAG_SIGNIN = "com.mitchell.gelani.Gelani_Misc.SIGN_IN";
    public final static String sEXECUTETAG_RESERVEACAB = "com.mitchell.gelani.Gelani_Misc.RESERVE_A_CAB";
    public final static String sPOST_REQUEST = "POST";

    public static final int iGPLAY_SERVICES_CONNECTION_REQUEST_CODE = 10101;
    public static final int iGPLAY_SERVICES_CONNECTION_STATUS_CODE = 20202;
    public final static int iOneSecond = 1000;
    public final static int iUPDATE_INTERVAL = 30 * iOneSecond;
    public final static int iFASTEST_UPDATE_INTERVAL = 10 * iOneSecond;

    public final static int iLOCATION_DIALOG = 101;
}
