package com.mitchell.gelani;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mitchell.gelani.Gelani_Background.DB_Worker;
import com.mitchell.gelani.Gelani_Misc.Gelani_Codes;

public class SignInActivity extends AppCompatActivity {

    private EditText edEmail;
    private EditText edPassword;

    private TextInputLayout tilEmail;
    private TextInputLayout tilPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        initializeVariablesAndUIObjects();
    }

    /**
     * THis method declares/initializes variables used in this class,
     * and Objects used in the UI of this Activity.
     *
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        tilEmail = (TextInputLayout) this.findViewById(R.id.tilSIEmail);
        tilPassword = (TextInputLayout) this.findViewById(R.id.tilSIPassword);

        edEmail = (EditText) this.findViewById(R.id.edSIEmail);
        edPassword = (EditText) this.findViewById(R.id.edSIPassword);

        Button btnSignIn = (Button) this.findViewById(R.id.btnSignIn);
        Button btnSignUp = (Button) this.findViewById(R.id.btnSISignUp);
        btnSignIn.setOnClickListener(clkSignIn);
        btnSignUp.setOnClickListener(clkSignIn);
    }

    private void codeToValidateUserData() {

        String sTILError;

        if (edEmail.getText().toString().equalsIgnoreCase("")) {
            sTILError = this.getResources().getString(R.string.tileEmptyField) + " your Email Address!";
            tilEmail.setError(sTILError);

            edEmail.requestFocus();
        } else if (edPassword.getText().toString().equalsIgnoreCase("")) {
            sTILError = this.getResources().getString(R.string.tileEmptyField) + " your Password!";
            tilPassword.setError(sTILError);

            edPassword.requestFocus();
        } else {
            tilEmail.setError("");
            tilPassword.setError("");

            codeToSignInUser();
        }
    }

    private void codeToSignInUser() {

        String sEmailAddress = edEmail.getText().toString();
        String sPassword = edPassword.getText().toString();

        DB_Worker clsWorker = new DB_Worker(SignInActivity.this);
        clsWorker.execute(Gelani_Codes.sEXECUTETAG_SIGNIN, sEmailAddress, sPassword);

        edEmail.setText("");
        edPassword.setText("");
    }


    /**
     * View.OnClickListener for clickable views on this Activity.
     * It first checks which View has been clicked,
     * by comparing view IDs.
     * R.id.btnSignIn :: Sign In button in Sign In Activity
     * Signs in User.
     *
     * R.id.btnSignUp :: Sign Up button in Sign In Activity.
     * Starts Sign Up Activity.
     *
     * implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkSignIn = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int iViewId = v.getId();
            switch (iViewId) {
                case R.id.btnSignIn:
                    codeToValidateUserData();
                    break;

                case R.id.btnSISignUp:
                    Intent inStartSignUp = new Intent(SignInActivity.this, SignUpActivity.class);
                    finish();
                    startActivity(inStartSignUp);
                    break;
            }
        }
    };
}
