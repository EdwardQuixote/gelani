package com.mitchell.gelani;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.mitchell.gelani.Gelani_Background.DB_Worker;
import com.mitchell.gelani.Gelani_Dialogs.G_Dialogs;
import com.mitchell.gelani.Gelani_Misc.Gelani_Codes;

import java.util.Random;

public class GetStartedActivity extends AppCompatActivity {

    private ScrollView scvGetStarted;

    private TextInputLayout tilDestination;
    private TextInputLayout tilPickUpPoint;
    private TextInputLayout tilContact;

    private EditText edDestination;
    private EditText edPickUpPoint;
    private EditText edContact;

    private Spinner spnCabCompany;
    private Spinner spnTypeOfCab;

    private RadioButton radCash;
    private RadioButton radCard;

    private String[] saryCabCompanies;
    private String[] saryTypeOfCab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to initialize Class Variables,
     * and objects used in the UI of this Activity.
     *
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        saryCabCompanies = this.getResources().getStringArray(R.array.saryCabCompanies);
        saryTypeOfCab = this.getResources().getStringArray(R.array.saryTypeOfCab);

        ArrayAdapter<String> adpCabCompanies = new ArrayAdapter<>(GetStartedActivity.this, android.R.layout.simple_spinner_dropdown_item, saryCabCompanies);
        ArrayAdapter<String> adpTypeOfCab = new ArrayAdapter<>(GetStartedActivity.this, android.R.layout.simple_spinner_dropdown_item, saryTypeOfCab);

        spnCabCompany = (Spinner) this.findViewById(R.id.spnGSCabCompany);
        spnTypeOfCab = (Spinner) this.findViewById(R.id.spnGSTypeOfCab);
        spnCabCompany.setAdapter(adpCabCompanies);
        spnTypeOfCab.setAdapter(adpTypeOfCab);

        scvGetStarted = (ScrollView) this.findViewById(R.id.scvGetStarted);

        tilDestination = (TextInputLayout) this.findViewById(R.id.tilGSDestination);
        tilPickUpPoint = (TextInputLayout) this.findViewById(R.id.tilGSPickUpPoint);
        tilContact = (TextInputLayout) this.findViewById(R.id.tilGSContact);

        edDestination = (EditText) this.findViewById(R.id.edGSDestination);
        edPickUpPoint = (EditText) this.findViewById(R.id.edGSPickUpPoint);
        edContact = (EditText) this.findViewById(R.id.edGSContact);

        radCash = (RadioButton) this.findViewById(R.id.radGSCash);
        radCard = (RadioButton) this.findViewById(R.id.radGSCard);

        Button btnReserve = (Button) this.findViewById(R.id.btnGSReserve);
        Button btnCancel = (Button) this.findViewById(R.id.btnGSCancel);
        btnReserve.setOnClickListener(clkGetStarted);
        btnCancel.setOnClickListener(clkGetStarted);
    }

    /**
     * Method to validate what the user provided.
     * Checks if there are any blanks.
     * If none, it calls method to reserve cab.
     *
     * Called in this.clkGetStarted.onClick();
     */
    private void codeToValidateUserData() {

        if (
                !edDestination.getText().toString().equalsIgnoreCase("") &&
                !edPickUpPoint.getText().toString().equalsIgnoreCase("") &&
                !edContact.getText().toString().equalsIgnoreCase("") &&
                        (radCash.isChecked() || radCard.isChecked())) {

            codeToReserveCab();
        } else {
            String sAlertMessage;

            //  Destination EditText
            if (edDestination.getText().toString().equalsIgnoreCase("")) {
                sAlertMessage = this.getResources().getString(R.string.tileEmptyField) + " Destination.";
                tilDestination.setError(sAlertMessage);
            } else {
                tilDestination.setError("");
            }

            //  Pick-up Point EditText
            if (edPickUpPoint.getText().toString().equalsIgnoreCase("")) {
                sAlertMessage = this.getResources().getString(R.string.tileEmptyField) + " Pick-up Point.";
                tilPickUpPoint.setError(sAlertMessage);
            } else {
                tilPickUpPoint.setError("");
            }

            //  Contact EditText
            if (edContact.getText().toString().equalsIgnoreCase("")) {
                sAlertMessage = this.getResources().getString(R.string.tileEmptyField) + " Contact.";
                tilContact.setError(sAlertMessage);
            } else {
                tilContact.setError("");
            }

            //  Mode of Payment RadioButtons
            if (!radCash.isChecked() && !radCard.isChecked()) {
                sAlertMessage = this.getResources().getString(R.string.tileEmptyField) + " Mode of Payment.";
                codeToGenerateSnackBarDialogs(sAlertMessage);
            }
        }
    }

    /**
     * Method to reserve Cab that user has requested.
     * It saves user-provided data to the database.
     *
     * Called in this.codeToValidateUserData();
     */
    private void codeToReserveCab() {

        String sDestination;
        String sPickUpPoint;
        String sContact;
        String sModeOfPayment = "";
        String sCabCompany = "";
        String sTypeOfCab = "";
        String sCharges;

        sDestination = edDestination.getText().toString();
        sPickUpPoint = edPickUpPoint.getText().toString();
        sContact = edContact.getText().toString();
        if (radCash.isChecked()) {
            sModeOfPayment = this.getResources().getString(R.string.sCash);
        } else if (radCard.isChecked()) {
            sModeOfPayment = this.getResources().getString(R.string.sCard);
        }

        switch (spnCabCompany.getSelectedItemPosition()) {
            case 0: //  Any
                sCabCompany = saryCabCompanies[0];
                break;

            case 1: //  Maridady Cabs
                sCabCompany = saryCabCompanies[1];
                break;

            case 2: //  Delights Cabs
                sCabCompany = saryCabCompanies[2];
                break;

            case 3: //  JatCo Cabs
                sCabCompany = saryCabCompanies[3];
                break;
        }

        switch (spnTypeOfCab.getSelectedItemPosition()) {
            case 0: //  Any
                sTypeOfCab = saryTypeOfCab[0];
                break;

            case 1: //  Personal Car
                sTypeOfCab = saryTypeOfCab[1];
                break;

            case 2: //  PSV TODO: Change this after talking to Mitch about it.
                sTypeOfCab = saryTypeOfCab[2];
                break;

            case 3: //  Special Service
                sTypeOfCab = saryTypeOfCab[3];
                break;
        }

        Random ranCharges = new Random();
        int iCharges = ranCharges.nextInt(1000);
        sCharges = String.valueOf(iCharges);

        DB_Worker clsWorker = new DB_Worker(GetStartedActivity.this);
        clsWorker.execute(Gelani_Codes.sEXECUTETAG_RESERVEACAB, sDestination, sPickUpPoint, sCabCompany, sTypeOfCab, sModeOfPayment, sCharges, sContact);

        G_Dialogs clsDialogs = new G_Dialogs(GetStartedActivity.this);
        clsDialogs.codeToGenerateReceiptAlertDialog(sDestination, sPickUpPoint, sCabCompany, sTypeOfCab, sCharges, sModeOfPayment);

        edDestination.setText("");
        edPickUpPoint.setText("");
        edContact.setText("");
    }

    /**
     * This method generates/shows a Snackbar with a specific alert message,
     * each time it's called.
     *
     * Called in this.codeToValidateUserData();
     *
     * @param alertMessage (String)
     */
    private void codeToGenerateSnackBarDialogs(String alertMessage) {

        Snackbar snkAlert = Snackbar.make(scvGetStarted, alertMessage, Snackbar.LENGTH_LONG);
        snkAlert.show();
    }


    /**
     * View.OnClickListener interface to consume clicks on Views,
     * in this Activity.
     * It takes actions on CLicks depending on the specific view clicked.
     * It checks the Id of the view to know which view was clicked.
     * case R.id.btnGSReserve::
     * This reserves a cab for the user and stores the data in a database.
     *
     * case R.id.btnGSCancel::
     * This finishes the activity and cancels the current activity.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkGetStarted = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int iViewId = v.getId();
            switch (iViewId) {
                case R.id.btnGSReserve:
                    codeToValidateUserData();
                    break;

                case R.id.btnGSCancel:
                    finish();
                    break;
            }
        }
    };
}
