package com.mitchell.gelani;

import android.app.Application;
import android.content.res.Configuration;

/**
 * Application Class for this App.
 *
 * Created by Edward Quixote
 * on 17/08/2016,
 * at 05:32PM.
 */
public class Gelani extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
