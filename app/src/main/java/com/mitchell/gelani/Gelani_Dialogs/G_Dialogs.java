package com.mitchell.gelani.Gelani_Dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.mitchell.gelani.Gelani_Misc.Gelani_Codes;
import com.mitchell.gelani.R;

/**
 * Created by Edward Quixote on 28/03/2016.
 */
public class G_Dialogs {

    private Context coxContext;

    private TextView txtFromTo;
    private TextView txtCabCompany;
    private TextView txtTypeOfCab;
    private TextView txtCharges;
    private TextView txtModeOfPayment;

    private AppCompatRatingBar rateCabRating;

    private ProgressDialog pdgProgress;

    public G_Dialogs(Context context) {
        this.coxContext = context;
    }

    private View codeToCreateReceiptDialogView() {

        View vDialogView = LayoutInflater.from(coxContext).inflate(R.layout.dialog_receipt, null);

        txtFromTo = (TextView) vDialogView.findViewById(R.id.txtReceiptFromTo);
        txtCabCompany = (TextView) vDialogView.findViewById(R.id.txtReceiptCabCompany);
        txtTypeOfCab = (TextView) vDialogView.findViewById(R.id.txtReceiptTypeOfCab);
        txtCharges = (TextView) vDialogView.findViewById(R.id.txtReceiptCharges);
        txtModeOfPayment = (TextView) vDialogView.findViewById(R.id.txtReceiptModeOfPayment);

        rateCabRating = (AppCompatRatingBar) vDialogView.findViewById(R.id.rateReceiptRating);

        return vDialogView;
    }

    /**
     * Code to generate the AlertDialog to display a receipt,
     * after the user has reserved a Cab.
     *
     * @return (adgbldBuilder.create()) AlertDialog.
     */
    public void codeToGenerateReceiptAlertDialog(String destination, String pickUpPoint, String cabCompany, String typeOfCab, String charges, String modeOfPayment) {

        AlertDialog.Builder adgbldBuilder = new AlertDialog.Builder(coxContext);
        adgbldBuilder.setTitle(coxContext.getResources().getString(R.string.sYourCab));
        adgbldBuilder.setView(codeToCreateReceiptDialogView());
        adgbldBuilder.setPositiveButton(android.R.string.ok, null);

        AlertDialog adgReceipt = adgbldBuilder.create();

        txtFromTo.setText(destination + " to " + pickUpPoint);
        txtCabCompany.setText(cabCompany);
        txtTypeOfCab.setText(typeOfCab);
        txtCharges.setText("KES " + charges);
        txtModeOfPayment.setText("via " + modeOfPayment + ".");

        rateCabRating.setRating(3.0f);

        adgReceipt.show();
    }

    public void codeToCreateAlertDialogs(int whichDiag, String message) {

        AlertDialog.Builder adgbldAlertBuilder;
        AlertDialog adgAlert;

        switch (whichDiag) {
            case Gelani_Codes.iLOCATION_DIALOG:
                adgbldAlertBuilder = new AlertDialog.Builder(coxContext);
                adgbldAlertBuilder.setTitle(coxContext.getString(R.string.sLocation));
                adgbldAlertBuilder.setMessage(message);
                adgbldAlertBuilder.setPositiveButton(R.string.sSettings, clkLocationPositive);
                adgbldAlertBuilder.setNegativeButton(R.string.sCancel, clkLocationNegative);

                adgAlert = adgbldAlertBuilder.create();
                adgAlert.show();
        }
    }

    /**
     * This method creates a ProgressDialog.
     * Mostly ProgressDialogs are used for Loading.
     *
     * @param message (String)
     */
    public void codeToCreateProgressDialogs(String message) {

        pdgProgress = new ProgressDialog(coxContext);
        pdgProgress.setMessage(message);
        pdgProgress.setCanceledOnTouchOutside(false);
        pdgProgress.setCancelable(false);
        pdgProgress.show();

    }

    /**
     * Method to dismiss any showing ProgressDialog.
     * First Checks if the ProgressDialog is showing.
     */
    public void codeToDismissProgressDialogs() {

        if (pdgProgress != null) {
            pdgProgress.dismiss();
        }
    }


    /**
     * Private DialogInterface.OnClickListener Interface.
     * Handles Click on Positive button of Location Dialog.
     * Starts Location Settings Activity.
     *
     * Implemented in codeToCreateAlertDialogs();
     */
    private DialogInterface.OnClickListener clkLocationPositive = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {

            Intent inStartSettings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            coxContext.startActivity(inStartSettings);

        }

    };

    /**
     * Private DialogInterface.OnClickListener Interface.
     * Handles Click on Negative Button for Location Dialog, Case 101:
     * Dismisses the Dialog.
     *
     * Implemented in codeToCreateAlertDialogs();
     */
    private DialogInterface.OnClickListener clkLocationNegative = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }

    };
}
